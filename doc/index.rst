##################
Currency RS Module
##################

The *Currency RS Module* adds the `Serbian National Bank <https://nbs.rs/>` as
a source for currency exchange rates.

.. toctree::
   :maxdepth: 2

   configuration
   usage
   design
